var Z_MAX = 6;
var PROVISIONAL_K = 30;
var LOW_RATING_K = 15;
var HIGH_RATING_K = 10;


/*
    Calculates the new score for the participants of a match.

    winnerRating - double - the winner's current rating
    winnerProvisional - boolean -if the winner is in the provisional period
    loserRating - double - the loser's current rating
    loserProvisional - boolean - if the winner is in the provisional period

    returns - an object with the new ratings, winnerRating and loserRating

 */
function elo(winnerRating, winnerProvisional, loserRating, loserProvisional){
    var winnerHasHighestRating = winnerRating>=loserRating;
    var winnerExpectedScore;
    if(winnerHasHighestRating){
        winnerExpectedScore = expectedScore(winnerRating-loserRating);
    } else {
        winnerExpectedScore = 1 - expectedScore(loserRating-winnerRating);
    }

    var winnerIncrease = getK(winnerRating, winnerProvisional) * (1 - winnerExpectedScore);
    var loserDecrease = getK(loserRating,loserProvisional) * (0-(1-winnerExpectedScore));

    var winnerNewRating = winnerRating + winnerIncrease;
    var loserNewRating = loserRating + loserDecrease;

    return {winnerRating: winnerNewRating, loserRating: loserNewRating}
}

/*
    Returns the K for the player with the current rating and if he is in a provisional period or not
 */
function getK(rating, provisional){
    //K = 30 (was 25) for a player new to the rating list until s/he has completed events with a total of at least 30 games.[16]
    //K = 15 as long as a player's rating remains under 2400.
    //K = 10 once a player's published rating has reached 2400, and s/he has also completed events with a total of at least 30 games. Thereafter it remains permanently at 10.
    if(provisional){
        return PROVISIONAL_K;
    }

    if(rating<2400){
        return LOW_RATING_K;
    } else {
        return HIGH_RATING_K;
    }
}


/*
    Returns the expected score for the player with the highest rating, based on the difference in ratings between two players.
    The variance for Elo is 200.
 */
function expectedScore(ratingDifference){
    return normalDist(0, 1, ratingDifference/200);
}
function testExpectedScore(){
    assertExpected(0.0, 0.5);
    assertExpected(20.0, 0.5398);
    assertExpected(40.0, 0.5793);
    assertExpected(60.0, 0.6179);
    assertExpected(80.0, 0.6554);
    assertExpected(100.0, 0.6915);
    assertExpected(120.0, 0.7257);
    assertExpected(122.0, 0.7291);
    assertExpected(158.9, 0.7865);
}

function assertExpected(difference, expected){
    var actual = expectedScore(difference);
    assertEquals(expected, actual);
}

function testNewRatings(){
    assertNewRating(1400.0, 1300.0, 1409.3, 1290.7);
    assertNewRating(2100.0, 2100.0, 2115.0, 2085.0);
    assertNewRating(2100.0, 2000.0, 2109.3, 1990.7);
    assertNewRating(1900.0, 2100.0, 1925.2, 2074.8);
}

function assertNewRating(winnerRating, loserRating, winnerExpectedNewRating, loserExpectedNewRating){
    PROVISIONAL_K = 30;
    LOW_RATING_K = 30;
    HIGH_RATING_K = 24;
    newRatings = elo(winnerRating, false, loserRating, false);
    actualWinnerNewRating = mround(newRatings.winnerRating);
    actualLoserNewRating = mround(newRatings.loserRating);
    assertEquals(winnerExpectedNewRating, actualWinnerNewRating);
    assertEquals(loserExpectedNewRating, actualLoserNewRating);
}

function mround(rating){
    rating = (rating * 10);
    rating = Math.round(rating);
    rating = (rating/10);
    return rating.toFixed(1);
}

function assertEquals(expected, actual){
    if(expected!=actual){
        console.log("Expected: "+ expected + ", but got: " + actual);
    } else {
        console.log("Passed");
    }

}

/*
	From http://easycalculation.com/statistics/normal-distribution.php
*/
function normalDist(mean,sd,val)
{
    var below = 0;
    mean = parseFloat(mean);
    sd = parseFloat(sd);
    val = parseFloat(val);

    if(val<mean)
    {
        var y=mean-val;
        var z=y/sd;
        below=zscore(z)*10000/10000;
    }
    else if(val == mean)
    {
        below=0.5;
    }
    else if(val > mean)
    {
        var y=val-mean;
        var z=y/sd;
        below=Math.round(zscore(z)*10000)/10000;
    }

    return below;
}

/*
	From http://easycalculation.com/statistics/normal-distribution.php

*/
function zscore(z)
{
    var y, x, w;
    if (z == 0.0)
    {
        x = 0.0;
    }
    else
    {
        y = 0.5 * Math.abs(z);
        if (y > (Z_MAX * 0.5))
        {
            x = 1.0;
        }
        else if (y < 1.0)
        {
            w = y * y;
            x = ((((((((0.000124818987 * w
                - 0.001075204047) * w + 0.005198775019) * w
                - 0.019198292004) * w + 0.059054035642) * w
                - 0.151968751364) * w + 0.319152932694) * w
                - 0.531923007300) * w + 0.797884560593) * y * 2.0;
        }
        else
        {
            y -= 2.0;
            x = (((((((((((((-0.000045255659 * y
                + 0.000152529290) * y - 0.000019538132) * y
                - 0.000676904986) * y + 0.001390604284) * y
                - 0.000794620820) * y - 0.002034254874) * y
                + 0.006549791214) * y - 0.010557625006) * y
                + 0.011630447319) * y - 0.009279453341) * y
                + 0.005353579108) * y - 0.002141268741) * y
                + 0.000535310849) * y + 0.999936657524;
        }
    }
    return z > 0.0 ? ((x + 1.0) * 0.5) : ((1.0 - x) * 0.5);
}

